import React from 'react';
import {render, screen} from '@testing-library/react';
import {App} from './App';

test('renders Tech Corner branding', () => {
    render(<App/>);
    const linkElement = screen.getByText(/Tech Corner/i);
    expect(linkElement).toBeInTheDocument();
});
