import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faUserPlus } from "@fortawesome/free-solid-svg-icons";
import { NavLink } from "react-router-dom";
import './NavBar.scss'

export class NavBar extends Component {
    public render(): JSX.Element {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary user-select-none">
                <span className="navbar-brand techBrand">
                    <img src="/crystals.svg" alt="crystal icon"/>
                    <span>Tech Corner</span>
                </span>
                <button
                    className="navbar-toggler collapsed"
                    type="button"
                    data-toggle="collapse"
                    data-target=".nav-collapsable"
                    aria-controls="tech-main-navbar"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse nav-collapsable">
                    <ul className="navbar-nav mr-auto">
                        {NavBar.renderMenu()}
                    </ul>
                    <span>
                        {NavBar.renderAuth()}
                    </span>
                </div>
            </nav>
        );
    }

    private static renderMenu(): JSX.Element {
        return (
            <>
                {NavBar.renderMenuItem("/", "Home")}
                {NavBar.renderMenuItem("/ffxiv", "FFXIV Crafting")}
            </>
        );
    }

    private static renderMenuItem(path: string, title: string): JSX.Element {
        return (
            <NavLink
                activeClassName="active"
                exact={true}
                className="nav-item nav-link"
                to={path}
            >
                {title}
            </NavLink>
        );
    }

    private static renderAuth(): JSX.Element {
        return (
            <ul className="navbar-nav mr-auto">
                <span className="nav-item nav-link iconizedItem">
                    <span><FontAwesomeIcon icon={faUserPlus} /></span>
                    <span>Register</span>
                </span>
                <span className="nav-item nav-link iconizedItem">
                    <span><FontAwesomeIcon icon={faUser} /></span>
                    <span>Login</span>
                </span>
            </ul>
        );
    }
}
