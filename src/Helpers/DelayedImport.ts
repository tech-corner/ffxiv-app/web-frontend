import React from "react";
import {delay} from "./Delay";

const lazyImport = (imp: () => any, delayMs: number = 0) => {
    return React.lazy(async () => {
        if(delayMs) await delay(delayMs);
        return imp();
    });
}

export default lazyImport;