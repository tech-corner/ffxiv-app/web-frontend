import React, {Component} from "react";

export default class Home extends Component {
    public render() {
        return (
            <div className="row justify-content-center align-items-center h-100 w-100">
                <div>
                    <h3 className="text-center">Summary</h3>
                    <p className="text-center">
                        The page is intended as testing playground of various frontend and backend
                        technologies.
                    </p>
                </div>
            </div>
        );
    }
}