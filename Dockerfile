FROM node:lts-slim AS compile-image

WORKDIR build
COPY . .
RUN yarn install && yarn build

FROM nginx:stable-alpine AS run-image
RUN echo -e "types {\n"\
"          application/wasm    wasm;\n"\
"      }\n"\
"      server {\n"\
"          listen 80;\n"\
"          root /opt/frontend;\n"\
"          try_files \$uri /index.html;\n"\
"      }" > /etc/nginx/conf.d/default.conf
COPY --from=compile-image build/build/ /opt/frontend/

